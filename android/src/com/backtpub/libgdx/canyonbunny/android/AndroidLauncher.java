package com.backtpub.libgdx.canyonbunny.android;

import android.os.Bundle;

import com.backtpub.libgdx.canyonbunny.CanyonBunnyMain;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new CanyonBunnyMain(), config);
	}
}
