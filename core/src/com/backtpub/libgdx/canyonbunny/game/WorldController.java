package com.backtpub.libgdx.canyonbunny.game;

import com.backtpub.libgdx.canyonbunny.game.objects.BunnyHead;
import com.backtpub.libgdx.canyonbunny.game.objects.Feather;
import com.backtpub.libgdx.canyonbunny.game.objects.GoldCoin;
import com.backtpub.libgdx.canyonbunny.game.objects.Rock;
import com.backtpub.libgdx.canyonbunny.screens.DirectedGame;
import com.backtpub.libgdx.canyonbunny.screens.MenuScreen;
import com.backtpub.libgdx.canyonbunny.screens.transitions.ScreenTransition;
import com.backtpub.libgdx.canyonbunny.screens.transitions.ScreenTransitionSlide;
import com.backtpub.libgdx.canyonbunny.util.CameraHelper;
import com.backtpub.libgdx.canyonbunny.util.Constants;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by humb1t on 21.12.14.
 */
public class WorldController extends InputAdapter {
    private static final String TAG =
            WorldController.class.getName();
    public Level level;
    public int lives;
    public int score;
    public CameraHelper cameraHelper;
    public float livesVisual;
    public float scoreVisual;
    private float timeLeftGameOverDelay;
    //rectangles for collision detection
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();
    private DirectedGame game;

    public WorldController(DirectedGame game) {
        this.game = game;
        init();
    }

    public void update(float deltaTime) {
        handleDebugInput(deltaTime);
        if (isGameOver()) {
            timeLeftGameOverDelay -= deltaTime;
            if (timeLeftGameOverDelay < 0) {
                backToMenu();
            }
        } else {
            handleInputGame(deltaTime);
        }
        level.update(deltaTime);
        testCollisions();
        cameraHelper.update(deltaTime);
        if (!isGameOver() && isPlayerInWater()) {
            lives--;
            if (isGameOver()) {
                timeLeftGameOverDelay = Constants.TIME_DELAY_GAME_OVER;
            } else {
                initLevel();
            }
        }
        level.mountains.updateScrollPosition(
                cameraHelper.getPosition()
        );
        if (livesVisual > lives) {
            livesVisual = Math.max(lives, livesVisual - 1 * deltaTime);
        }
        if (scoreVisual < score) {
            scoreVisual = Math.min(score, scoreVisual + 250 * deltaTime);
        }
    }

    public boolean isGameOver() {
        return lives < 0;
    }

    public boolean isPlayerInWater() {
        return level.bunnyHead.position.y < -2;
    }

    private void onCollisionBunnyHeadWithRock(Rock rock) {
        BunnyHead bunnyHead = level.bunnyHead;
        float heightDifference = Math.abs(
                bunnyHead.position.y - (rock.position.y + rock.bounds.height)
        );
        if (heightDifference > 0.25f) {
            boolean hitLeftEdge = bunnyHead.position.x >
                    (rock.position.x + rock.bounds.width / 2.0f);
            if (hitLeftEdge) {
                bunnyHead.position.x = rock.position.x + rock.bounds.width;
            } else {
                bunnyHead.position.x = rock.position.x -
                        bunnyHead.bounds.width;
            }
            return;
        }
        switch (bunnyHead.jumpState) {
            case GROUNDED:
                break;
            case FALLING:
            case JUMP_FALLING:
                bunnyHead.position.y = rock.position.y
                        + bunnyHead.bounds.height
                        + bunnyHead.origin.y;
                bunnyHead.jumpState = BunnyHead.JUMP_STATE.GROUNDED;
                break;
            case JUMP_RISING:
                bunnyHead.position.y = rock.position.y
                        + bunnyHead.bounds.height
                        + bunnyHead.origin.y;
                break;
        }
    }

    private void onCollisionBunnyWithGoldCoin(GoldCoin goldCoin) {
        goldCoin.collected = true;
        score += goldCoin.getScore();
        Gdx.app.log(TAG, "Gold coin collected");
    }

    private void onCollisionBunnyWithFeather(Feather feather) {
        feather.collected = true;
        score += feather.getScore();
        level.bunnyHead.setFeatherPowerup(true);
        Gdx.app.log(TAG, "Feather collected");
    }

    private void testCollisions() {
        r1.set(level.bunnyHead.position.x,
                level.bunnyHead.position.y,
                level.bunnyHead.bounds.width,
                level.bunnyHead.bounds.height);
        //Test collision: Bunny Head <--> Rocks
        for (Rock rock : level.rocks) {
            r2.set(rock.position.x, rock.position.y,
                    rock.bounds.width, rock.bounds.height);
            if (!r1.overlaps(r2)) {
                continue;
            }
            onCollisionBunnyHeadWithRock(rock);
            //IMPORTANT: must do all collisions for valid edge testing
        }
        //Test collision: Bunny Head <--> Gold Coins
        for (GoldCoin goldCoin : level.goldCoins) {
            if (goldCoin.collected) {
                continue;
            }
            r2.set(goldCoin.position.x, goldCoin.position.y,
                    goldCoin.bounds.width, goldCoin.bounds.height);
            if (!r1.overlaps(r2)) {
                continue;
            }
            onCollisionBunnyWithGoldCoin(goldCoin);
            break;
        }
        //Test collision: Bunny Head <--> Feather
        for (Feather feather : level.feathers) {
            if (feather.collected) {
                continue;
            }
            r2.set(feather.position.x, feather.position.y,
                    feather.bounds.width, feather.bounds.height);
            if (!r1.overlaps(r2)) {
                continue;
            }
            onCollisionBunnyWithFeather(feather);
            break;
        }
    }

    private void initLevel() {
        score = 0;
        scoreVisual = score;
        level = new Level(Constants.LEVEL_01);
        cameraHelper.setTarget(level.bunnyHead);
    }

    private void init() {
        cameraHelper = new CameraHelper();
        lives = Constants.LIVES_START;
        livesVisual = lives;
        timeLeftGameOverDelay = 0;
        initLevel();
    }

    private void handleDebugInput(float deltaTime) {
        if (Gdx.app.getType() != Application.ApplicationType.Desktop)
            return;

        if (!cameraHelper.hasTarget(level.bunnyHead)) {
            //Camera controls (moves)
            float camMoveSpeed = 5 * deltaTime;
            float camMoveSpeedAccelerationFactor = 5;
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT))
                camMoveSpeed *= camMoveSpeedAccelerationFactor;
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                moveCamera(-camMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                moveCamera(camMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Input.Keys.UP))
                moveCamera(0, camMoveSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
                moveCamera(0, -camMoveSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.BACKSPACE))
                cameraHelper.setPosition(0, 0);
        }
        //Camera Controls (zoom)
        float camZoomSpeed = 1 * deltaTime;
        float camZoomSpeedAccelerationFactor = 5;
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT))
            camZoomSpeed *= camZoomSpeedAccelerationFactor;
        if (Gdx.input.isKeyPressed(Input.Keys.COMMA))
            cameraHelper.addZoom(camZoomSpeed);
        if (Gdx.input.isKeyPressed(Input.Keys.PERIOD)) cameraHelper.addZoom(
                -camZoomSpeed);
        if (Gdx.input.isKeyPressed(Input.Keys.SLASH)) cameraHelper.setZoom(1);
    }

    private void moveCamera(float x, float y) {
        x += cameraHelper.getPosition().x;
        y += cameraHelper.getPosition().y;
        cameraHelper.setPosition(x, y);
    }

    @Override
    public boolean keyUp(int keycode) {
        //Reset game world
        if (keycode == Input.Keys.R) {
            init();
            Gdx.app.debug(TAG, "Game world resetted");
        }
        //Toggle camera follow
        else if (keycode == Input.Keys.ENTER) {
            cameraHelper.setTarget(cameraHelper.hasTarget() ? null : level.bunnyHead);
            Gdx.app.debug(TAG, "Camera follow enabled: " + cameraHelper.hasTarget());
        } else if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
            backToMenu();
        }
        return false;
    }

    private void handleInputGame(float deltaTime) {
        if (cameraHelper.hasTarget(level.bunnyHead)) {
            //Player Movement
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                level.bunnyHead.velocity.x = -level.bunnyHead.terminalVelocity.x;
            } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                level.bunnyHead.velocity.x = level.bunnyHead.terminalVelocity.x;
            } else {
                //Execute auto-forward movement on non-desktop platform
                if (Gdx.app.getType() != Application.ApplicationType.Desktop) {
                    level.bunnyHead.velocity.x = level.bunnyHead.terminalVelocity.x;
                }
            }
            //Bunny jump
            if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
                level.bunnyHead.setJumping(true);
            } else {
                level.bunnyHead.setJumping(false);
            }
        }
    }

    private void backToMenu() {
        ScreenTransition transition = ScreenTransitionSlide.init(0.75f,
                ScreenTransitionSlide.RIGHT, false, Interpolation.sineIn);
        game.setScreen(new MenuScreen(game), transition);
    }
}
