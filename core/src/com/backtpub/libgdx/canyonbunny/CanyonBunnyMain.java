package com.backtpub.libgdx.canyonbunny;

import com.backtpub.libgdx.canyonbunny.game.Assets;
import com.backtpub.libgdx.canyonbunny.screens.DirectedGame;
import com.backtpub.libgdx.canyonbunny.screens.MenuScreen;
import com.backtpub.libgdx.canyonbunny.screens.transitions.ScreenTransition;
import com.backtpub.libgdx.canyonbunny.screens.transitions.ScreenTransitionSlice;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Interpolation;

/**
 * Created by humb1t on 21.12.14.
 */
public class CanyonBunnyMain extends DirectedGame {
	@Override
	public void create()
	{
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		//Load assets
		Assets.instance.init(new AssetManager());
        ScreenTransition transition = ScreenTransitionSlice.init(2,
                ScreenTransitionSlice.UP_DOWN, 10, Interpolation.pow5Out);
        setScreen(new MenuScreen(this), transition);
    }
}
