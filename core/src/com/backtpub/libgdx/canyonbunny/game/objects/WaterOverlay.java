package com.backtpub.libgdx.canyonbunny.game.objects;

import com.backtpub.libgdx.canyonbunny.game.Assets;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by humb1t on 08.01.15.
 */
public class WaterOverlay extends AbstractGameObject
{
	private TextureRegion regWaterOverlay;
	private float length;

	public WaterOverlay(float length)
	{
		this.length = length;
		init();
	}

	private void init()
	{
		dimension.set(length * 10, 3);
		regWaterOverlay =
				Assets.instance.levelDecoration.waterOverlay;
		origin.x = - dimension.x / 2;
	}

	@Override
	public void render(SpriteBatch batch)
	{
		TextureRegion reg = null;
		reg = regWaterOverlay;
		batch.draw(reg.getTexture(),
				position.x + origin.x, position.y + origin.x,
				origin.x, origin.y,
				dimension.x, dimension.y,
				scale.x, scale.y,
				rotation,
				reg.getRegionX(), reg.getRegionY(),
				reg.getRegionWidth(), reg.getRegionHeight(),
				false, false);
	}
}
