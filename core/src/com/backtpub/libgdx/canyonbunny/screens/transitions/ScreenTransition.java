package com.backtpub.libgdx.canyonbunny.screens.transitions;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by humb1t on 19/04/15.
 */
public interface ScreenTransition {
    public float getDuration();

    public void render(SpriteBatch batch,
                       Texture currScreen,
                       Texture nextScreen,
                       float alpha);
}
