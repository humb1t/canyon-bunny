package com.backtpub.libgdx.canyonbunny.desktop;

import com.backtpub.libgdx.canyonbunny.CanyonBunnyMain;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class DesktopLauncher {
    private static boolean rebuildAtlas = true;
    private static boolean drawDebugOutline = false;

	public static void main (String[] arg) {
        if (rebuildAtlas) {
            TexturePacker.Settings settings = new TexturePacker.Settings();
            settings.maxWidth = 1024;
            settings.maxHeight = 1024;
            settings.debug = drawDebugOutline;
            TexturePacker.process(settings, "assets-raw/images",
                    "images", "canyonbunny");
			TexturePacker.process(settings, "assets-raw/images-ui",
					"images", "canyonbunny-ui");
        }
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.height = 480;
        config.width = 800;
		config.title = "CanyonBunny";
		new LwjglApplication(new CanyonBunnyMain(), config);
	}
}
